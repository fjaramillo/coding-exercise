import lang from 'config/i18n.json'
const Utils = {}

function * generateID () {
    let i = 0
    while (true) {
        yield i++
    }
}

Utils.CONTROLS = {
    TEXT: 'textbox'
}

Utils.NOTIFICATION = {
    SUCCESS: 'success',
    ALERT: 'warning'
}

Utils.sort = (data, filterBy, order = 'asc') => {
    const {field, dataType} = filterBy
    let compare = (order === 'asc') ? (a, b) => (a > b ? 1 : -1) : (a, b) => (b > a ? 1 : -1)

    switch (dataType) {
    case 'phone':
        compare = (order === 'asc') ? (a, b) => (a - b) : (a, b) => (b - a)
        return data.sort((a, b) => {
            const numA = a[field].replace('+', '')
            const numB = b[field].replace('+', '')
            return compare(numA, numB)
        })
    case 'string':
    default:
        return data.sort((a, b) => compare(a[field].toLowerCase(), b[field].toLowerCase()))
    }
}

Utils.delete = (value, field, source) => {
    const idx = source.findIndex(row => row[field] === value)
    source.splice(idx, 1)
    return source
}

Utils.addJSON = (current, newInfo, id) => {
    newInfo.map(row => {
        const idx = current.findIndex(item => item[id] === row[id])
        if (idx < 0) {
            current.push(row)
        }
    })

    return current
}

Utils.updateJSON = (current, id, keyID, field, value) => {
    const idx = current.findIndex(row => row[keyID] === id)
    const item = current[idx]
    item[field] = value
    current.splice(idx, 1, item)
    return current
}
Utils.i18n = (key) => {
    let temp = null
    for (let x of key.split('.')) {
        temp = temp ? temp[x] : lang[x]
    }
    return temp
}

Utils.generator = generateID()

Utils.getId = () => Utils.generator.next().value

export default Utils
