class API {
    constructor (props) {
        this.name = 'Base API to Persist JSON'
        this.key = 'ABDC-ASDA-ERSD'
    }

    get () {
        return JSON.parse(window.localStorage.getItem(this.key))
    }

    save (json) {
        window.localStorage[this.key] = JSON.stringify(json)
        return true
    }

    put (item) {
        const temp = this.get()
        temp.push(item)
    }

    delete (id, field) {
        let temp = this.get().slice()
        const idx = temp.findIndex(row => row[field] === id)
        temp.splice(idx, 1)
        this.save(temp)
    }
}

export default API
