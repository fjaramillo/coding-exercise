import API from './api'

const fetch = {} // To avoid ESLint Problem
// NOTE: This API is just for EXAMPLE

class REST extends API {
    constructor (props) {
        super(props)
        this.name = 'REST API to Persist JSON'
        this.key = 'ABDC-ERTY-ERSD'
        this.endpoint = {
            'GET': '/employess',
            'POST': '/employees',
            'DELETE': '/employees',
            'PUT': '/employees/:id'
        }
    }

    get () {
        return fetch(this.endpoint.GET)
        // axios ?
        // jQuery ?
    }

    save (json) {
        return fetch('POST', this.endpoint.POST, json)
    }

    put (item) {
        return fetch('PUT', this.endpoint.PUT, item)
    }

    delete (id, field) {
        const url = this.endpoint.DELETE.replace(':id', id)
        return fetch('DELETE', url)
    }
}

export default REST
