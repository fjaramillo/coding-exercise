import React from 'react'
import ReactDOM from 'react-dom'
import App from './app'
import { Router, Route, IndexRoute, hashHistory } from 'react-router'
import DBView from 'views/db'
import Home from 'views/home'
import style from 'theme.css' 

ReactDOM.render(
    <Router history={hashHistory}>
        <Route path='/' component={App}>
            <IndexRoute component={Home} />
            <Route path='/db' component={DBView} />
        </Route>
    </Router>
, document.querySelector('#app'))

// This allow uglify to exlcude code from our bundle base on variables from DefinePlugin
// called as Code Elimination
if (DEVELOPMENT) {
    // Accepts Hot Reload from Webpack HMR
    if (module.hot) {
        module.hot.accept()
    }
}
