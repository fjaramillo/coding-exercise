import React, {Component} from 'react'
import API from 'libs/api'

export default class Login extends Component {
    constructor (props) {
        super(props)

        this.API = new API()
    }

    render () {
        return (
            <div>
                <div className='row'>
                    <div className='col-xs-12'>
                        <h1>DATABASE</h1>
                        <div className='code'>
                            <pre>{
                                JSON.stringify(this.API.get(), null, 2)
                            }</pre>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

Login.propTypes = {
    children: React.PropTypes.object
}
