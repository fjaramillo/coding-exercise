import React, {Component} from 'react'
import {GForm, FormGroup} from 'components/gform'
import Utils from 'libs/utils'
import Notification from 'components/notification'
import {GTable, GHeader} from 'components/gtable'
import JSONLoad from 'components/jsonLoad'
import API from 'libs/api'

const DATA = [
    {
        firstname: 'Fabian',
        surname: 'Alan',
        telephone: '+2345678',
        email: 'fab@fab.com'
    }, {
        firstname: 'Carlos',
        surname: 'Jaramillo 2',
        telephone: '145678342',
        email: 'carlos@fab2.com'
    }
]

class Home extends Component {
    constructor (props) {
        super(props)

        // To save the form fields, It's not in the state to avoid render on every
        // change
        this.newUser = {}
        // Reset data every time the component is loaded
        this.API = new API()
        this.API.save(DATA)

        this.state = {
            isValid: false,
            formKey: Utils.getId(),
            tableData: DATA
        }
        this.addUser = this.addUser.bind(this)
        this.fieldChange = this.fieldChange.bind(this)
        this.validate = this.validate.bind(this)
        this.LoadJSON = this.LoadJSON.bind(this)
        this.updateColumn = this.updateColumn.bind(this)
        this.syncJSON = this.syncJSON.bind(this)
    }

    addUser (evt) {
        evt.preventDefault()
        const form = this.refs.form
        if (form.isValid()) {
            const clone = this
                .refs
                .table
                .getData()
            const idx = clone.findIndex(usr => usr.email === this.newUser.email)

            if (idx >= 0) {
                this
                    .refs
                    .notification
                    .showMessage(Utils.NOTIFICATION.ALERT, Utils.i18n('PAGE_ADD_ERROR'))
            } else {
                clone.push(this.newUser)
                // reset the object
                this.newUser = {}
                this
                    .API
                    .save(clone)
                this.setState({
                    formKey: Utils.getId(),
                    tableData: clone
                }, () => {
                    this
                        .refs
                        .notification
                        .showMessage(Utils.NOTIFICATION.SUCCESS, Utils.i18n('PAGE_ADD_SUCCESS'))
                })
            }
        }
    }

    fieldChange (evt, instance) {
        // Perform Validation onChange per Field
        instance._hasError(!instance._validate())
        const {name} = instance.props
        this.newUser[name] = evt.target.value
    }

    validate (value, instance) {
        // TODO: Move this to Utils.js
        let regex = {
            'text': /^[A-Za-z0-9]{3,50}$/,
            'number': /^[+\d]+[0-9]{7,15}$/,
            'email': /^(([a-zA-Z0-9_\-.]+)@([a-zA-Z0-9_\-.]+)\.([a-zA-Z]{2,5}){1,25})+([;.](([a-zA-Z0-9_\-.]+)@{[a-zA-Z0-9_\-.]+0\.([a-zA-Z]{2,5}){1,25})+)*$/
        }
        return regex[instance.props.dataType].test(value)
    }

    LoadJSON (json) {
        if (json && json instanceof Object) {
            const newData = Utils.addJSON(this.refs.table.getData(), json, 'email')
            this.API.save(newData)

            this.setState({tableData: newData})
            this.refs.notification.showMessage(
                Utils.NOTIFICATION.SUCCESS, 
                Utils.i18n('LOAD_JSON_SUCCESS')
            )
        } else {
            this.refs.notification.showMessage(
                Utils.NOTIFICATION.ALERT, 
                Utils.i18n('LOAD_JSON_ERROR')
            )
        }
        
    }

    updateColumn (evt, instance, id, field, key) {
        const isValid = instance._validate()
        // Show error to user
        if (!isValid) {
            instance._hasError(false)
            this.refs.notification.showMessage(
                Utils.NOTIFICATION.ALERT, 
                Utils.i18n('TABLE_UPDATE_ERROR'), 
                true, 
                () => instance._focus(), 
                () => this.refs.table.cancelEdit()
            )
        } else { // Update Column
            const newData = Utils.updateJSON(this.refs.table.getData(), id, key, field, evt.target.value)
            this.API.save(newData)
            this.refs.notification.showMessage(
                Utils.NOTIFICATION.SUCCESS, 
                Utils.i18n('TABLE_UPDATE_SUCCESS'), 
                false, 
                () => this.setState({tableData: newData})
            )
        }
    }

    syncJSON (json) {
        this.API.save(json)
    }

    render () {
        const loadButton = <JSONLoad onLoad={this.LoadJSON} className='load-json pull-right' />
        return (
            <div className='container'>
                <div className='row top-form'>
                    <GForm
                      key={this.state.formKey}
                      ref='form'
                      columns={2}
                      onSubmit={this.addUser}
                      submitClass='add-btn pull-right'>
                        <FormGroup
                          name='firstname'
                          className='form-control'
                          dataType='text'
                          placeholder='First Name'
                          control={Utils.CONTROLS.TEXT}
                          onChange={this.fieldChange}
                          validate={this.validate} />
                        <FormGroup
                          name='surname'
                          className='form-control'
                          dataType='text'
                          placeholder='Surname'
                          control={Utils.CONTROLS.TEXT}
                          onChange={this.fieldChange}
                          validate={this.validate} />
                        <FormGroup
                          name='telephone'
                          className='form-control'
                          dataType='number'
                          placeholder='Telephone'
                          control={Utils.CONTROLS.TEXT}
                          onChange={this.fieldChange}
                          validate={this.validate} />
                        <FormGroup
                          name='email'
                          className='form-control'
                          dataType='email'
                          placeholder='Email'
                          control={Utils.CONTROLS.TEXT}
                          onChange={this.fieldChange}
                          validate={this.validate} />
                    </GForm>
                </div>
                <div className='row table-content'>
                    <GTable
                      ref='table'
                      data={this.state.tableData}
                      title={Utils.i18n('TABLE_TITLE')}
                      extraContainer={loadButton}
                      validate={this.validate}
                      onUpdate={this.updateColumn}
                      onDelete={this.syncJSON}>
                        <GHeader field='firstname' dataType='string' editType='text' sortable editable>First Name</GHeader>
                        <GHeader field='surname' dataType='string' editType='text' sortable editable>Surname</GHeader>
                        <GHeader field='telephone' dataType='phone' editType='number' sortable editable>Telephone</GHeader>
                        <GHeader
                          isKey
                          field='email'
                          dataType='string'
                          editType='email'
                          sortable
                          editable>Email</GHeader>
                    </GTable>
                </div>
                <Notification ref='notification' />
            </div>
        )
    }
}

export default Home

Home.propTypes = {
    children: React.PropTypes.object
}
