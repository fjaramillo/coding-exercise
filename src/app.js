import React, { Component, PropTypes } from 'react'
import Navigation from './components/nav'
import 'babel-polyfill'

class App extends Component {
    render () {
        return (
            <div className='container'>
                <div className='row'>
                    <Navigation />
                </div>
                <div className='row'>
                    {this.props.children}
                </div>
            </div>
        )
    }
}

App.propTypes = {
    children: PropTypes.any
}

export default App
