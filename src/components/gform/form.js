import React, { Component, PropTypes } from 'react';
import classnames from 'classnames'
import FormGroup from './formGroup'
import Utils from 'libs/utils'

class GForm extends Component {
    constructor (props) {
        super(props)

        this.state = {
            msgError: ''
        }
    }

    buildState () {
        
    }

    renderForm () {
        const colWidth = 12 / this.props.columns
        let result = []
        let id = 0

        // Add Reference for all fields
        let cols = this.props.children.map((field, i) => {
            const {name} = field.props
            const component = React.cloneElement(field, { ref: name })
            return <div key={i} className={`col-md-${colWidth}`}>{ component }</div>
        })
        
        // Divide fields in number of Columns
        while (cols.length > 0) { 
            const rows = cols.slice(0, this.props.columns)
            cols.splice(0, this.props.columns)
            result.push(
                <div key={id++} className='row'>{ 
                    rows
                }</div>
            )
        }
        return result;
    }

    renderButtons () {
        return <div className='row'>
            <div className='col-md-12'>
                <button className={classnames('btn btn-primary', this.props.submitClass)} onClick={e => this.props.onSubmit(e)}>{this.props.submitLabel}</button>
            </div>
        </div>
    }

    isValid () {
        let hasError = false
        Object.keys(this.refs).map((key, i) => {
            const instance = this.refs[key].refs[key]
            const isValid = instance._validate()
            
            if (!isValid) {
                instance._hasError(true)
                hasError = true
            }
        })
        this.setState({ msgError: hasError ? Utils.i18n('PAGE_ERROR') : '' })
        return !hasError;
    }

    render () {
        const error = this.props.showError ? <div className='row'>
            <p className='msgError pull-right'>{ this.state.msgError}</p>
        </div> : null

        return (
            <form className={this.props.className}>
                {this.renderForm()}
                {this.renderButtons()}
                {error}
            </form>
        );
    }
}

GForm.propTypes = {
    children: PropTypes.any,
    columns: PropTypes.oneOf([2,4,6]),
    onSubmit: PropTypes.func,
    submitLabel: PropTypes.string,
    submitClass: PropTypes.string,
    showError: PropTypes.bool,
    className: PropTypes.string
};

GForm.defaultProps = {
    columns: 2,
    submitLabel: 'Add',
    showError: true
};

export default GForm;