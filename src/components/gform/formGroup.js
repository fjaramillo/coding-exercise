import React, { Component, PropTypes } from 'react';
import TextBox from 'components/controls/textbox'
import Utils from 'libs/utils'


class FormGroup extends Component {
    renderControl () {
        const props = this.props

        switch (props.control) {
            case Utils.CONTROLS.TEXT:
            default:
                return <TextBox ref={props.name} 
                  name={props.name} 
                  dataType={props.dataType} 
                  className={props.className} 
                  change={props.onChange}
                  validate={props.validate} 
                  defaultValue={props.value}
                  placeholder={props.placeholder} />
            break;
        }
    }

    render () {
        const props = this.props
        return (
            <div className='form-group'>
                {props.label && <label htmlFor={props.name} >{props.label}</label> }
                {this.renderControl()}
            </div>
        )
    }
}

FormGroup.propTypes = {
    name: PropTypes.string.isRequired,
    label: PropTypes.string,
    value: PropTypes.string,
    placeholder: PropTypes.string,
    dataType: PropTypes.oneOf(['text', 'number', 'email', 'date']),
    className: PropTypes.string,
    control: PropTypes.oneOf(['textbox']).isRequired,
    validate: PropTypes.func,
    onChange: PropTypes.func
};

FormGroup.defaultProps = {
    type: 'text'
}

export default FormGroup;