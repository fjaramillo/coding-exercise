import React from 'react'
import {shallow, mount} from 'enzyme'
import chai from 'chai'
import sinon from 'sinon'
import chaiEnzyme from 'chai-enzyme'
import TextBox from './textbox'

const expect = chai.expect
chai.use(chaiEnzyme()) // We need to tell chai to use chaiEnzyme

describe('TextBox Component', () => {
    it('Render with Error', () => {
        try {
            const view = mount(<TextBox />)
        } catch(e) {
            const error = new Error('Name is required')
            expect(e.message).to.be.equals(error.message)
        }
    })

    it('Render with Basic Props', () => {
        const view = shallow(<TextBox name='name' />)
        expect(view.find('input')).to.have.lengthOf(1)
    })

    it('Render with Default Value', () => {
        const view = shallow(<TextBox name='name' defaultValue='Testing' />)
        expect(view.find('input')).to.have.value('Testing')
    })

    it('Render with Event Handlers', () => {
        const onChange = sinon.spy()
        const onBlur = sinon.spy()
        const onValidate = sinon.spy()

        const view = mount(<TextBox change={onChange} blur={onBlur} validate={onValidate}
          name='name' defaultValue='Testing' />)

        view.find('input').simulate('change', { target: { value: 'Testing' } })
        view.find('input').simulate('blur', { target: { value: 'Testing' } })

        expect(onChange.callCount).to.be.equal(1)
        expect(onBlur.callCount).to.be.equal(1)

        view.instance()._validate()

        expect(onValidate.callCount).to.be.equal(1)
    })

    it('Render with Has Error Function', () => {
        const view = mount(<TextBox name='name' defaultValue='Testing' />)
        view.instance()._hasError(true)
        expect(view.find('.error')).to.have.lengthOf(1)
        view.instance()._hasError(false)
        expect(view.find('.error')).to.have.lengthOf(0)
    })
})
