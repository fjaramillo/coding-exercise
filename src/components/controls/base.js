import React, { Component, PropTypes } from 'react'

class BaseControl extends Component {
    constructor (props) {
        super(props)

        this._onChange = this._onChange.bind(this)
        this._onBlur = this._onBlur.bind(this)
        this.state = {
            hasError: false
        }

        if (!this.props.name) {
            throw new Error('Name is required')
        }
    }

    componentDidMount () {
        if (this.props.initFocus) { 
            this._focus()
        }
    }

    componentDidUpdate () {
        if (this.state.hasError) {
            this._focus()
        }
    }

    _validate () {
        return this.props.validate(this.refs.control.value, this)
    }

    _onChange (evt) {
        this.props.change && this.props.change(evt, this)
    }

    _onBlur (evt) {
        this.props.blur && this.props.blur(evt, this)
    }

    _hasError (value) {
        this.setState({ hasError: value })
    }

    _focus () {
        this.refs.control.focus()
    }
}

BaseControl.propTypes = {
    name: PropTypes.string.isRequired,
    defaultValue: PropTypes.string,
    dataType: PropTypes.oneOf(['text', 'number', 'email', 'date']),
    className: PropTypes.string,
    validate: PropTypes.func,
    change: PropTypes.func,
    blur: PropTypes.func,
    initFocus: PropTypes.bool
}

export default BaseControl
