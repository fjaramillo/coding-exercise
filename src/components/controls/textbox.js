import React from 'react'
import BaseControl from './base'
import classnames from 'classnames'

class TextBox extends BaseControl {
    render () {
        const {change, blur, initFocus, validate, className, dataType, ...props} = this.props
        const classes = classnames(className, { 'error': this.state.hasError })

        return <input ref='control' type='text' className={classes} {...props}
          onChange={(e) => this._onChange(e)}
          onBlur={(e) => this._onBlur(e)}
                />
    }
}

export default TextBox
