import React, { Component, PropTypes } from 'react';
import Utils from 'libs/utils'
import classnames from 'classnames'

class Notification extends Component {
    constructor (props) {
        super(props)

        this.state = {
            open : false,
            msg: '',
            level: '',
            showCancel: false
        }
    }

    showMessage (type , msg, showCancel = false, onConfirm, onCancel) {
        this.setState({
            open: true,
            msg: msg,
            level: type,
            showCancel: showCancel,
            onConfirm: onConfirm,
            onCancel: onCancel
        })
    }

    onConfirm (e) {
        typeof this.state.onConfirm === 'function' ? this.state.onConfirm() : null
        this.close()
    }

    onCancel (e) {
        typeof this.state.onCancel === 'function' ? this.state.onCancel() : null
        this.close()
    }

    close (evt) {
        this.setState({
            open: false
        })    
    }

    render () {
        const btnCancel = this.state.showCancel ? <button className='btn btn-danger pull-left' onClick={(e) => this.onCancel(e)}>Cancel</button> : null
        const classPanel = classnames('panel', `panel-${this.state.level}`)
        return this.state.open ? (
            <div className='notify-container'>
                <div className={classPanel}>
                    <div className='panel-heading'>
                        <h3 className='panel-title'>{ Utils.i18n(`PAGE_TITLE_${this.state.level.toUpperCase()}`) }</h3>
                    </div>
                    <div className='panel-body'>
                        { this.state.msg }
                    </div>
                    <div className='panel-footer'>
                        <button className='btn btn-primary pull-right' onClick={(e) => this.onConfirm(e)}>Confirm</button>
                        {btnCancel}
                        <div className='clearfix' />
                    </div>
                </div>
            </div>
        ) : null
    }
}

Notification.propTypes = {

};

export default Notification;