import React, { Component } from 'react';
import { Link } from 'react-router'

class Nav extends Component {
    render () {
        return (
            <nav className='navbar navbar-default'>
                <div className='container-fluid'>
                    <div className='navbar-header'>
                        <button type='button' className='navbar-toggle collapsed' data-toggle='collapse' data-target='#bs-example-navbar-collapse-1' aria-expanded='false'>
                            <span className='sr-only'>Toggle navigation</span>
                            <span className='icon-bar' />
                            <span className='icon-bar' />
                            <span className='icon-bar' />
                        </button>
                        <a className='navbar-brand' href='javascript:;'>GameSys</a>
                    </div>

                    <div className='collapse navbar-collapse' id='bs-example-navbar-collapse-1'>
                        <ul className='nav navbar-nav'>
                            <li><Link to='/' activeClassName='active' onlyActiveOnIndex>Employess</Link></li>
                        </ul>
                        <ul className='nav navbar-nav navbar-right'>
                            <li><Link to='/db' activeClassName='active'>JSON</Link></li>
                        </ul>
                    </div>
                </div>
            </nav>
        );
    }
}

export default Nav;