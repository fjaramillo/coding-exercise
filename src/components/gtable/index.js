import GTable from './gtable'
import GHeader from './gheader'

export {
    GTable,
    GHeader
}