import React, { Component, PropTypes } from 'react';
import clasnames from 'classnames'

class GHeader extends Component {
    _onSort (evt) {
        const order = this.props.sortActive && this.props.sortOrder === 'asc' ? 'des' : 'asc'
        this.props.onSort(evt, this.props, order)
    }

    render () {
        const { sortable, field, dataType, sortActive, sortOrder } = this.props
        const sort = sortable && sortActive ? <span className='caret' /> : null
        const classes = clasnames({'sortable': sortable, 'dropup': sortActive && sortOrder === 'asc'})
        return (
            <th onClick={(e) => this._onSort(e)} className={classes} style={{width: this.props.width}}>
                {sort}
                {this.props.children}
            </th>
        )
    }
}

GHeader.propTypes = {
    field: PropTypes.string,
    dataType: PropTypes.string,
    sortable: PropTypes.bool,
    editable: PropTypes.bool,
    onSort: PropTypes.func,
    width: PropTypes.string,
    sortOrder: PropTypes.oneOf(['asc','des']),
    sortActive: PropTypes.bool,
    editType: PropTypes.oneOf(['text', 'number', 'email', 'date']),
    children: PropTypes.any
}

GHeader.defaultProps = {
    
}

export default GHeader;