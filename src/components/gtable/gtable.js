import React, { Component, PropTypes } from 'react';
import Utils from 'libs/utils'
import TextBox from 'components/controls/textbox'

class GTable extends Component {
    constructor (props) {
        super(props)

        const defaultSort = this.initTable(props)
        
        this.state = Object.assign({
            editMode: false,
            rowSelected: -1,
        }, defaultSort )

        this.updateData = this.updateData.bind(this)
    }

    initTable (props) {
        this.columns = []
        
        props.children.map((td, i) => {
            // collect information about the columns
            this.columns.push(td.props)

            // verify keyColumn
            this.keyColumn = td.props.isKey ? td.props.field : null
        })

        // If there is not keyColumn throw an error
        if (!this.keyColumn) {
            throw new Error('Key Column is required')
        }

        const colSort = props.sortOptions ? this.columns.find( col => col.field === props.sortOptions.sortBy ) : {}
        const sortedData = colSort.field ? Utils.sort(props.data, colSort, props.sortOptions.sortOrder) : props.data

        return {
            currentSort : colSort.sortBy || '',
            sortOrder : props.sortOrder || '',
            dataTable: sortedData
        }
    }

    componentWillReceiveProps (nextProps) {
        const newState = this.initTable(nextProps)

        this.setState( Object.assign(newState, { 
            editMode: false,
            rowSelected: -1,
            editField: null})
        )
    }

    renderHeader () {
        return <thead>
            <tr>{
                    this.props.children.map((td, i) => {
                        const {onDelete, ...props} = td

                        const isActiveSort = td.props.field === this.state.currentSort
                        return React.cloneElement(td, { 
                            key: i, 
                            ref: td.props.field,
                            onSort: this._onSort.bind(this),
                            sortActive: isActiveSort,
                            sortOrder: isActiveSort ? this.state.sortOrder : null
                            
                        })
                    })
                }
                <th style={{width: '80px'}}>
                    { Utils.i18n('DELETE') }
                </th>
            </tr>
        </thead>
    }

    allowEdit (id, field) {
        this.setState({
            editMode: true,
            rowSelected: id,
            editField: field
        })
    }

    renderEditMode (id, column, value) {
        const {field, editType} = column
        return <TextBox  name={`edit-${field}`} 
          dataType={editType}  
          blur={this.updateData}
          validate={this.props.validate}
          className='form-control'
          defaultValue={value}
          initFocus />
    }

    updateData (evt, instance) {
        const { rowSelected, editField } = this.state
        this.props.onUpdate(evt, instance, rowSelected, editField, this.keyColumn)
    }

    cancelEdit () {
        this.setState({
            editMode: false,
            rowSelected: -1,
            editField: null
        })
    }

    renderBody () {
        const isEmpty = this.state.dataTable.length === 0
        return !isEmpty ? <tbody>{    
            // Iteration of all the data
            this.state.dataTable.map((row, i) => {
                // Creation of Columns                         
                const tds = this.columns.map((col, y) => {
                    const { editable, field } = col
                    const value = row[field]
                    // edit only the row selected in the column selected
                    const changeMode = this.state.editMode && 
                                       this.state.editField === field &&
                                       this.state.rowSelected === row[this.keyColumn]

                    const output =  changeMode ? this.renderEditMode(row[this.keyColumn], col, value) : value
                    const editOnClick = editable && !this.state.editMode ? {
                        onClick : () => this.allowEdit(row[this.keyColumn], field),
                        className : 'edit-column'
                    } : null
                    return (
                        <td {...editOnClick} key={`${i}-${y}`} style={{width: col.width}}>{output}</td>
                    )
                })

                return <tr key={i}>
                    {tds}
                    <td style={{width: '80px'}}>
                        <button className='btn btn-delete' onClick={(e) => this._onDelete(e, this.keyColumn, row[this.keyColumn])}><i className='glyphicon glyphicon-trash' /></button>
                    </td>
                </tr>
            })
        }
        </tbody> : this.renderEmptyBody()
    }

    renderEmptyBody () {
        return <tr>
            <td colSpan={this.columns.length + 1}>
                <div className='empty-table'>
                    <p>{this.props.noData}</p>
                </div>
            </td>
        </tr>
    }

    _onDelete (evt, field, value) {
        const newSource = Utils.delete(value, field, this.state.dataTable.slice())
        typeof this.props.onDelete === 'function' ? this.props.onDelete(newSource) : null
        this.setState({
            dataTable: newSource
        })
    }

    _onSort (evt, column, order) {
        const sortedData = Utils.sort(this.state.dataTable, column, order)
        this.setState({
            sortOrder : order,
            currentSort : column.field,
            dataTable: sortedData
        })
    }

    getData () {
		return this.state.dataTable.slice()
	}

    render () {
        return (
            <div className='table-container'>
                <div className='row'>
                    <div className='col-md-4'>
                        <h1>{this.props.title}</h1>
                    </div>
                    <div className='col-md-8'>
                        {this.props.extraContainer}
                    </div>          
                </div>
                <div className='row'>
                    <div className='col-md-12'>
                        <table className='table table-bordered table-striped'>
                            {this.renderHeader()}
                            {this.renderBody()}
                        </table>
                    </div>
                </div>
            </div>
        );
    }
}

GTable.propTypes = {
    title: PropTypes.string,
    data: PropTypes.array,
    extraContainer: PropTypes.any,
    onDelete: PropTypes.func,
    sortOptions: PropTypes.shape({
        sortBy: PropTypes.string,
        sortOrder: PropTypes.oneOf(['asc','des']),
        sortIcon: PropTypes.string
    }),
    noData: PropTypes.string,
    validate: PropTypes.func,
    onUpdate: PropTypes.func,
    onDelete: PropTypes.func,
    children: PropTypes.any
};

GTable.defaultProps = {
    noData: Utils.i18n("TABLE_NOT_DATA")
}

export default GTable;