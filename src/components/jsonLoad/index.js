import React, { Component, PropTypes } from 'react';
import Utils from 'libs/utils'
import Notification from 'components/notification'

class JSONLoad extends Component {
    
    loadFile (evt) {
        const files = this.refs.inputFile.files;
        // avoind procesing empty files
        if (files.length === 0) {
            return null
        }

        const reader = new FileReader();

        reader.onload = (e) => { 
            try {
                const result = JSON.parse(e.target.result)
                this.refs.inputFile.value = ''
                this.props.onLoad(result)
            } catch (e) {
                this.props.onLoad(false)
            }
            
        }

        reader.readAsText(files.item(0));
    }

    openFile (evt) {
        this.refs.inputFile.click()
    }

    render () {
        return ( 
            <div className={this.props.className}>
                <button onClick={(e) => this.openFile(e)} className='btn btn-primary'>{Utils.i18n('LOAD_BUTTON')}</button>
                <input ref='inputFile' type='file' onChange={(e) => this.loadFile(e)} className='hide' />
                <Notification ref='popup' />
            </div>
        );
    }
}

JSONLoad.propTypes = {
    onLoad: PropTypes.func,
    className: PropTypes.string
};

export default JSONLoad;