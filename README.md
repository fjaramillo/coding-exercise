# GameSys Code Test

React Application based on GameSys [Requirements](Requirements.pdf)

## Install

```bash
npm install
```

## Dev

```bash
npm run dev
```

## Production

```bash
npm run build
```

## Test

```bash
npm run test
```

## Coverage

```bash
npm run test:coverage
```

## ESlint

```bash
npm run eslint
```

## License

MIT. Copyright (c) [Fabian Jaramillo](https://www.linkedin.com/in/fabjaror)
