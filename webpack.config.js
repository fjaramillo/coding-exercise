var path = require('path')
var webpack = require('webpack')
var ExtractText = require('extract-text-webpack-plugin')
var HtmlPlugin = require('html-webpack-plugin'
)
var isDev = process.env.NODE_ENV === 'DEVELOPMENT'
var isProd = process.env.NODE_ENV === 'PRODUCTION'

var entries = isDev ? [
    'webpack/hot/dev-server',
    'webpack-dev-server/client?http://localhost:8080'
] : []

entries.push('./src/index.js')

var plugins = isDev ? [
    new webpack.HotModuleReplacementPlugin()
] : [
    new webpack.optimize.UglifyJsPlugin(),
    new ExtractText('style-[contentHash:10].css'),
    new HtmlPlugin({
        template: 'index.template.html'
    })
]

plugins.push(new webpack.DefinePlugin({
    DEVELOPMENT: JSON.stringify(isDev),
    PRODUCTION: JSON.stringify(isProd)
}))

var cssIdentifier = isProd ? '[hash:base64:10]' : '[path][name]---[local]'
var cssLoader = isProd ? ExtractText.extract({
    loader: 'css-loader?localIdentName=' + cssIdentifier
}) : ['style-loader', 'css-loader?localIdentName=' + cssIdentifier]

module.exports = {
    externals: {
        'jquery': 'jQuery', // Load jquery from the global location and don't include it into the bundle.
        'react/addons': true, // Do not include these externals in the bundle
        'react/lib/ExecutionEnvironment': true,
        'react/lib/ReactContext': true
    },
    devtool: 'source-map', // Allow Source Map to Debug
    entry: entries,
    plugins: plugins,
    resolve: {
        modules: [ path.resolve(__dirname, './src'), path.resolve('./node_modules') ],
        extensions: ['.js']
    },
    module: {
        loaders: [{
            test: /\.js$/,
            loaders: ['babel-loader'],
            exclude: '/(node_modules|spec/.js$)/'
        },
        {
            test: /\.js$/,
            enforce: 'pre',
            exclude: /(node_modules|\.spec\.js)/,
            use: [
                {
                    loader: 'eslint-loader',
                    options: {
                        failOnWarning: false,
                        failOnError: false,
                        emitWarning: true
                    }
                }
            ]
        },
        {
            test: /\.(png|jpg|gif)$/,
            loaders: ['url-loader?limit=15000&name=images/[hash:8].[ext]'],
            exclude: '/node_modules/'
        },
        {
            test: /\.(css)$/,
            loaders: cssLoader,
            exclude: '/node_modules'
        }]
    },
    output: {
        path: path.join(__dirname, 'dist'),
        publicPath: isProd ? '/' : '/dist/',
        filename: isProd ? 'bundle-[hash:12].min.js' : 'bundle.js'
    }
}
